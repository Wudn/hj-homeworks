'use strict';

function init() {
    let canvas = document.querySelector('canvas'),
        h = parseInt(canvas.clientHeight),
        w = parseInt(canvas.clientWidth),
        PI = Math.PI,
        ctx = canvas.getContext('2d'),
        starsColors = ['#ffffff', '#ffe9c4', '#d4fbff'];
    
    function getRandomFloat(min, max) {
        return Math.random() * (max - min) + min;
    }
        
    function randomInteger(min=200, max=400) {
        var rand = min - 0.5 + Math.random() * (max - min + 1)
        rand = Math.round(rand);
        return rand;
    }

    function randomColor() {
        let randomNumber = randomInteger(0,2);
        return starsColors[randomNumber];
    }

    // console.log(canvas);
    // console.log('h', h);
    // console.log('w', w);
    // console.log(ctx);

    ctx.fillStyle = '#000000';
    ctx.fillRect(0,0,w,h);

    function renderStars(starsCount=0) {
        console.log(`Количество звёзд ${starsCount}`);
        for(let i = 0; i < starsCount; i++) {
            // console.log(i);
            ctx.beginPath();
            ctx.arc(randomInteger(0, w), randomInteger(0, h), Number((Math.random()+0.1).toFixed(1)), 0, 2 * PI);
            ctx.fillStyle = randomColor();
            ctx.globalAlpha = getRandomFloat(0.8, 1);
            ctx.fill();
        }

    }

    renderStars(randomInteger());

    canvas.addEventListener('click', (event) => {
        ctx.clearRect(0, 0, w, h);
        ctx.fillStyle = '#000000';
        ctx.fillRect(0,0,w,h);
        let starsCount = randomInteger();
        // console.log('Отрисовал звездное небо');
        // console.log(`Количество звёзд ${starsCount}`)
        renderStars(starsCount);
        // console.log('x', event.layerX);
        // console.log('y', event.layerY);
    });
}

document.addEventListener('DOMContentLoaded', init);