'use strict';

// https://codepen.io/Disabled/pen/KbEzmQ

function init() {
    let canvas = document.querySelector('#draw'),
        ctx = canvas.getContext('2d'),
        curves = [],
        drawing = false,
        radiusCur,
        hueCur,
        shrink = false,
        needsRepaint = false,
        wW,
        wH;
    const radius = changeRadius();
    const hue = changeHue(); 

    function clearCtx() {
        ctx.clearRect(0, 0, wW, wH);
        curves = [];
        needsRepaint = true;
    }

    function circle(point) {
        ctx.beginPath();
        ctx.arc(point[0], point[1], point[3] / 2, 0, 2 * Math.PI);
        ctx.fillStyle = `hsl(${point[2]}, 100%, 50%)`;
        ctx.fill();
    }

    function smoothCurveBetween(p1, p2) {
        const cp = p1.map((coord, idx) => (coord + p2[idx]) / 2);

        ctx.quadraticCurveTo(p1[0], p1[1], cp[0], cp[1]);
        ctx.lineWidth = p1[3];
        ctx.strokeStyle = `hsl(${p1[2]}, 100%, 50%)`;
        ctx.stroke();
        
    }

    function smoothCurve(points) {
        for (let i = 1; i < points.length-1; i++) {
            ctx.beginPath();
            ctx.lineJoin = 'round';
            ctx.lineCap = 'round';

            ctx.moveTo(points[i][0], points[i][1]);
            smoothCurveBetween(points[i], points[i + 1]);
        }
    }

    function makePoint(x, y) {
        return [x, y, hueCur, radiusCur];
    }

    canvas.addEventListener('mousedown', (evt) => {
        drawing = true;

        const curve = [];
        
        curve.push(makePoint(evt.offsetX, evt.offsetY, hueCur, radiusCur));
        
        curves.push(curve);
        needsRepaint = true;
    });

    canvas.addEventListener('mouseup', (evt) => {
        drawing = false;
    });

    canvas.addEventListener('mouseleave', (evt) => {
        drawing = false;
    });

    canvas.addEventListener('mousemove', (evt) => {
        if(drawing) {

            const point = makePoint(evt.offsetX, evt.offsetY);
            curves[curves.length - 1].push(point);
            needsRepaint = true;
        }
    });

    canvas.addEventListener('dblclick', (evt) => {
        drawing = false;
        clearCtx();
    });

    function resize() {
        if (needsRepaint) {
            repaint();
            needRepaint = false;
        }
        wW = window.innerWidth;
        wH = window.innerHeight;

        canvas.width = wW;
        canvas.height = wH;
        canvas.style.backgroundColor = '#ffffff';
    }

    function repaint() {
        ctx.clearRect(0, 0, wW, wH);
        
        curves.forEach(curve => {

            circle(curve[0]);
            smoothCurve(curve);
        });
    }

    function changeRadius() {
        const radiusMin = 5;
        const radiusMax = 100;
        
        let foo = true;

        radiusCur = radiusMax;
        
        return function () {
                    if (radiusCur < radiusMax && foo) {
                        return radiusCur++;
                    } else {
                        foo = false;
                    }
                
                    if (radiusCur > radiusMin && !foo) {
                        return radiusCur--;
                    } else {
                        foo = true;
                    }
                }
    }

    function changeHue() {
        const hueMin = 0;
        const hueMax = 359;
        hueCur = hueMin

        return function () {
                    shrink ? hueCur-- : hueCur++;

                    if (hueCur < hueMin) {
                    hueCur = hueMax;
                    }

                    if (hueCur > hueMax) {
                        hueCur = hueMin;
                    }

                    return hueCur;
                }
    }

    function tick() {
        radius();
        hue();
        if (needsRepaint) {
            repaint();
            needsRepaint = false;
        }
        window.requestAnimationFrame(tick);
    }

    tick();

    document.body.style.overflow = 'hidden';

    resize();

    // console.log('sW', screen.width);
    // console.log('sH', screen.height);

    // console.log('wW', wW);
    // console.log('wH', wH);

    // console.log('cW', document.body.clientHeight);
    // console.log('cH', document.body.clientHeight);

    window.addEventListener('resize', resize);
}

document.addEventListener('DOMContentLoaded', init);