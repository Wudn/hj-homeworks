'use strict';

function init() {
    const conn = new WebSocket('wss://neto-api.herokuapp.com/mouse');
    const canvas = document.querySelector('canvas');

    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    function action(event) {
        let object = {};
   
        object.x = event.clientX;
        object.y = event.clientY;
        // object.color = '#D974DB';
        // object.radius = getRandomArbitrary(1, 16);

        conn.send(JSON.stringify(object));
    }   

    canvas.addEventListener('click', action);
    conn.addEventListener('open', () => {
        console.log('Соединение установлено');
    });
    conn.addEventListener('message', (event) => {
        console.log(JSON.parse(event.data));
    });
    conn.addEventListener('error', (error) => {
        console.error(error.data);
    });
    // conn.addEventListener('close', (event) => {
    //     console.log(event.data);
    // });
    window.addEventListener('beforeunload', () => { 
        conn.close(1000, 'Работа закончена');
    }); 
    showBubbles(conn);
}

document.addEventListener('DOMContentLoaded', init);