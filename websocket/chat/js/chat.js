'use strict';

function init() {
    const conn = new WebSocket('wss://neto-api.herokuapp.com/chat');
    let chatEl = document.querySelector('div.chat'),
        chatStatusEl = chatEl.querySelector('.chat-status'),
        msgBoxForm = chatEl.querySelector('form.message-box'),
        msgInput = msgBoxForm.querySelector('input.message-input'),
        msgSubmit = msgBoxForm.querySelector('button.message-submit'),
        msgContentEl = chatEl.querySelector('div.messages-content'),
        msgTemplatesEl = chatEl.querySelector('div.messages-templates');
    
    conn.addEventListener('open', () => {
        console.log('Соединение установлено');
        chatStatusEl.textContent = chatStatusEl.dataset.online;
        msgSubmit.removeAttribute('disabled');

        let template = msgTemplatesEl.querySelector('div.message-status'),
            dupNode = template.cloneNode(true);
        dupNode.firstElementChild.textContent = 'Пользователь появился в сети';
        msgContentEl.appendChild(dupNode);
    });

    conn.addEventListener('message', (event) => {
        if (event.data === '...') {
        } else {
                // msgTextEl = msgEl.querySelector('span.message-text'),
                // msgTimestamp = '23:24';
        
            let template = msgTemplatesEl.querySelector('div[class="message"]'),
                dupNode = template.cloneNode(true),
                date = new Date(),
                hours = (date.getHours()<10?'0':'') + date.getHours(),
                minutes = (date.getMinutes()<10?'0':'') + date.getMinutes();

            dupNode.querySelector('span.message-text').textContent = event.data;
            dupNode.querySelector('div.timestamp').textContent = `${hours}:${minutes}`;
            msgContentEl.appendChild(dupNode);
        }
    });

    conn.addEventListener('close', () => {
        msgSubmit.setAttribute('disabled', 'disabled');
    });

    conn.addEventListener('error', (error) => {
        console.error(error.data);
    })

    function sendMEssage(event) {
        event.preventDefault();
        let personalText = msgInput.value;
        conn.send(personalText);
        msgInput.value = '';

        let template = msgTemplatesEl.querySelector('div.message-personal'),
                dupNode = template.cloneNode(true),
                date = new Date(),
                hours = (date.getHours()<10?'0':'') + date.getHours(),
                minutes = (date.getMinutes()<10?'0':'') + date.getMinutes();
       
        dupNode.querySelector('span.message-text').textContent = personalText;
        dupNode.querySelector('div.timestamp').textContent = `${hours}:${minutes}`;
        msgContentEl.appendChild(dupNode);
    }

    msgSubmit.addEventListener('click', sendMEssage);

   
    window.addEventListener('beforeunload', () => {

        conn.close(1000, 'Работа закончена');
    });
}

document.addEventListener('DOMContentLoaded', init);

