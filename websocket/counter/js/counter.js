'use strict';

function init() {
    const conn = new WebSocket('wss://neto-api.herokuapp.com/counter');
    const counterEl = document.querySelector('span.counter');
    const errorsEl = document.querySelector('output.errors');

    conn.addEventListener('open', () => {
        console.log('Соединение установлено');
    });

    conn.addEventListener('message', (event) => {
        let data = JSON.parse(event.data);

        if (data.connections) {
            counterEl.textContent = data.connections;
        }
        if (data.errors) {
  
            errorsEl.textContent = data.errors;
        }
    });

    conn.addEventListener('error', (error) => {
        console.error(error.data);
    });

    window.addEventListener('beforeunload', () => {
        conn.close(1000, 'Работа закончена');
    });
}

document.addEventListener('DOMContentLoaded', init);