'use strict';
let slider,
    slides,
    sliderNav,
    prevBtn,
    nextBtn,
    firstBtn,
    lastBtn,
    activeElement,
    nextEl,
    disabledText = 'disabled';

function updateControls() {     
    activeElement.previousElementSibling ? firstBtn.classList.remove(disabledText) : firstBtn.classList.add(disabledText);
    activeElement.previousElementSibling ? prevBtn.classList.remove(disabledText) : prevBtn.classList.add(disabledText);
    
    activeElement.nextElementSibling ? nextBtn.classList.remove(disabledText) : nextBtn.classList.add(disabledText);
    activeElement.nextElementSibling ? lastBtn.classList.remove(disabledText) : lastBtn .classList.add(disabledText);
}

function setSlide(event) {
    event.preventDefault();
    let target = event.target;
    if(target.classList.contains(disabledText)) {
        return;
    }
    let action = target.dataset.action;
    activeElement.classList.remove('slide-current');  
    
    switch (action) { 
        case 'last':
            if(slides.lastElementChild) {
                activeElement = slides.lastElementChild;
            }
            break; 
        case 'prev': 
            if(activeElement.previousElementSibling) {
                activeElement = activeElement.previousElementSibling;
            }
            break; 
        case 'next': 
            if(activeElement.nextElementSibling) {
                activeElement = activeElement.nextElementSibling;
            }
            break; 
        case 'first': 
            if(slides.firstElementChild) {
                activeElement = slides.firstElementChild;
            }
            break;
    }

    if (activeElement) {
        activeElement.classList.add('slide-current');
        updateControls();
    }
    
}

function init() {
    slider = document.querySelector('.slider')
    slides = slider.querySelector('.slides');
    sliderNav = slider.querySelector('.slider-nav');
    prevBtn = sliderNav.querySelector('a[data-action="prev"]');
    nextBtn = sliderNav.querySelector('a[data-action="next"]');
    firstBtn = sliderNav.querySelector('a[data-action="first"]');
    lastBtn = sliderNav.querySelector('a[data-action="last"]');

    sliderNav.addEventListener('click', setSlide);

    activeElement = slides.firstElementChild;
    activeElement.classList.add('slide-current');
    updateControls();

}

document.addEventListener('DOMContentLoaded', init);