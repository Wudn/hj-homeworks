'use strict';

let tabs = document.querySelector('#tabs');
let tabsNav = tabs.querySelector('.tabs-nav')
let tabsContent = tabs.querySelector('.tabs-content');

let activeEl;

function updateTabs(event) {
    activeEl.classList.add('ui-tabs-active');
    Array.from(tabsContent.children).forEach((article) => {
        if (!activeEl.firstElementChild.classList.contains(article.dataset.tabIcon)) {
            article.classList.add('hidden');
        } else {
            article.classList.remove('hidden');
        }
    });
}

function setActiveEl(event) {
    activeEl.classList.remove('ui-tabs-active');
    activeEl = event.currentTarget;
    updateTabs();
}

function init() {
    console.log('fe:', tabsNav.firstElementChild);
    let clone = tabsNav.firstElementChild;
    Array.from(tabsContent.children).forEach((article) => {
        let tab = tabsNav.appendChild(clone.cloneNode(true));
        tab.firstElementChild.textContent = article.dataset.tabTitle;
        tab.firstElementChild.classList.add(article.dataset.tabIcon);
        tab.addEventListener('click', setActiveEl);
    });
    tabsNav.removeChild(clone);
    activeEl = tabsNav.firstElementChild;
    updateTabs()
}

document.addEventListener('DOMContentLoaded', init);