'use strict';

function init() {
    let todoList = document.querySelector('.todo-list');
    let inputs = todoList.querySelectorAll('input');
    let doneSection = todoList.querySelector('.done');
    let undoneSection = todoList.querySelector('.undone');

    Array.from(inputs).forEach(input => {
        input.addEventListener('click', () => {
            if (input.checked) {
                console.log(input);
                doneSection.appendChild(input.parentElement);
            } else {
                undoneSection.appendChild(input.parentElement);
            }
        });
    });
}

document.addEventListener('DOMContentLoaded', init);