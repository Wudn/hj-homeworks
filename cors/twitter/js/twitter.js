'use strict';

function init() {
    let wallpaperImg = document.querySelector('[data-wallpaper]'),
        usernameEl = document.querySelector('[data-username]'),
        descriptionEl = document.querySelector('[data-description]'),
        picImg = document.querySelector('[data-pic]'),
        tweetsEl = document.querySelector('[data-tweets]'),
        followersEl = document.querySelector('[data-followers]'),
        followingEl = document.querySelector('[data-following]');

    function showProfile(data) {
        console.log(data);
        wallpaperImg.src = data.wallpaper;
        usernameEl.textContent = data.username;
        descriptionEl.textContent = data.description;
        picImg.src = data.pic;
        tweetsEl.textContent = data.tweets;
        followersEl.textContent = data.followers;
        followingEl.textContent = data.following;
    }

    function randName() {
        return 'd' + Math.floor(Math.random() * 1000001);
    }

    function loadData(url) {
        const functionName = randName(); // 'showProfile';
        return new Promise((done, fail) => {
            window[functionName] = done;
            const script = document.scripts[0].cloneNode(); 
            script.src = `${url}?jsonp=${functionName}`;
            document.body.appendChild(script);

            done(JSON.parse(`{"username":"@carlf","description":"Carl Fredricksen is the protagonist in Up. He also appeared in Dug's Special Mission as a minor character.","tweets":2934,"followers":1119,"following":530,"wallpaper":"https://neto-api.herokuapp.com/hj/4.1/twitter/up.jpg","pic":"https://neto-api.herokuapp.com/hj/4.1/twitter/carl.jpg"}`));
        });
    }   

    loadData('https://neto-api.herokuapp.com/twitter/jsonp')
    .then(showProfile);
    

    // setInterval(() => console.log(p), 1000);


    // fetch('https://neto-api.herokuapp.com/twitter/json', {
    //     method: 'GET',
    // })
    // .then((res) => {
    //     if (res.status <= 200 && res.status < 300) {
    //         return res;
    //     }
    //     throw new Error(res.statusText);
    // })
    // .then((res) => res.json())
    // .then((data) => {
    //     console.log('d', data);
    // })
    // .catch((error) => {
    //     console.error(error);
    // });
}

document.addEventListener('DOMContentLoaded', init);