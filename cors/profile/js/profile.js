'use strict';

function init() {
    let content = document.querySelector('.content'),
        dataNameEl = content.querySelector('[data-name]'),
        descriptionEl = content.querySelector('[data-description]'),
        picEl = content.querySelector('[data-pic]'),
        positionEl = content.querySelector('[data-position]'),
        technologiesEl = content.querySelector('[data-technologies]'),
        followingEl = content.querySelector('[data-following]');

    // console.log(content);
    // console.log(dataNameEl);
    // console.log(descriptionEl);
    // console.log(picEl);
    // console.log(positionEl);
    // console.log(technologiesEl);
    // console.log(followingEl);

    function showProfile(data) {
        dataNameEl.textContent = data.name;
        descriptionEl.textContent = data.description;
        picEl.src = data.pic;
        positionEl.textContent = data.position;
        renderTechnologies(JSON.parse(`["django","python","codepen","javascript_badge","gulp","angular","sass"]`));
        content.style.display = 'initial';
        
    }

    function renderTechnologies(data) {
        data.forEach(technology => {
            console.log(technology);
            let technologyEl = document.createElement('span');
            technologyEl.classList.add('devicons', `devicons-${technology}`);
            technologiesEl.appendChild(technologyEl);
        });
        
    }
    
    function randName() {
        return 'd' + Math.floor(Math.random()*1000001);
    }
    
    function loadData(url) {
        const functionName = randName(); // 'showProfile';
        return new Promise((done, fail) => {
            window[functionName] = done;
            const script = document.scripts[0].cloneNode(); 
            script.src = `${url}?jsonp=${functionName}`;
            document.body.appendChild(script);
            done(JSON.parse(`{"id":90210,"name":"Francesco Moustache","position":"Python Ninja","description":"Lived all my life on the top of mount Fuji, learning the way to be a Ninja Dev.","pic":"https://neto-api.herokuapp.com/hj/4.1/profile/128.jpg"}`));
        });
    }
    
    loadData('https://neto-api.herokuapp.com/profile/me')
    .then(showProfile);
}

document.addEventListener('DOMContentLoaded', init);
