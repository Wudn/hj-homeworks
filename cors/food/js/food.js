'use strict';

function init() {
    let picEl = document.querySelector('[data-pic]'),
        titleEl = document.querySelector('[data-title ]'),
        ingredientsEl = document.querySelector('[data-ingredients]'),
        ratingEl = document.querySelector('[data-rating]'),
        starEl = document.querySelector('[data-star]'),
        votesEl = document.querySelector('[data-votes]'),
        consumersEl = document.querySelector('[data-consumers]');
    
    // console.log(picEl);
    // console.log(titleEl);
    // console.log(ingredientsEl);
    // console.log(ratingEl);
    // console.log(starEl);
    // console.log(votesEl);
    // console.log(consumersEl);

    function showRating(data) {
        let rating = Number(data.rating).toFixed(2),
            votes = `(${data.votes} оценок)`;
        ratingEl.textContent = rating;
        votesEl.textContent = votes;
    }

    function showConsumers(data) {
        let consumers = data.consumers;
        consumers.forEach(consumer => {
            let imgEl = document.createElement('img');
            imgEl.src = consumer.pic;
            imgEl.setAttribute('title', consumer.name);
            consumersEl.appendChild(imgEl);
        });
        let spanEl = document.createElement('span');
        spanEl.textContent = '(+20)';
        consumersEl.appendChild(spanEl);
    }

    function showRecipeInfo(data) {
        picEl.style.backgroundImage = `url(${data.pic})`;
        titleEl.textContent = data.title;
        ingredientsEl.textContent = data.ingredients.join(',')+'.';
        showRating(JSON.parse(`{"rating":9.1,"votes":320}`));
        showConsumers(JSON.parse(`{"consumers":[{"name":"Аманда","pic":"https://neto-api.herokuapp.com/hj/4.1/food/user1.jpg"},{"name":"Виктория","pic":"https://neto-api.herokuapp.com/hj/4.1/food/user2.jpg"},{"name":"Надежда","pic":"https://neto-api.herokuapp.com/hj/4.1/food/user3.jpg"},{"name":"Михаил","pic":"https://neto-api.herokuapp.com/hj/4.1/food/user4.jpg"}],"total":23}`));
    }
  
    function randName() {
        return 'd' + Math.floor(Math.random()*1000001);
    }
    
    function loadData(url) {
        const functionName = randName(); // 'showProfile';
        return new Promise((done, fail) => {
            window[functionName] = done;
            const script = document.scripts[0].cloneNode(); 
            script.src = `${url}?jsonp=${functionName}`;
            document.body.appendChild(script);
            done(JSON.parse(`{"id":42,"title":"Макарун","pic":"https://neto-api.herokuapp.com/hj/4.1/food/product.jpg","ingredients":["cахар","яичные белки","масло","соль","корица"]}`));
        });
    }
    
    loadData('https://neto-api.herokuapp.com/food/42')
    .then(showRecipeInfo);




}

document.addEventListener('DOMContentLoaded', init);