'use strict';

const req = new XMLHttpRequest();
let contentEl;
let iFrameEl;

function onError() {
    console.log('Извините, возникла сетевая ошибка.');
}

function onLoad() {
    console.log('Контент загружен.');
    let res = req.responseText;
    contentEl.innerHTML(res);
}

function getContent() {
    event.preventDefault();
    let url = this.href
    req.open('GET', url);
    req.send();
    iFrameEl.src = url;
}

function init() {
    let links = document.querySelectorAll('nav a');
    iFrameEl = document.createElement('iframe');
    iFrameEl.setAttribute('frameBorder', '0');
    for (let link of links) {
        if (link.classList.contains('active')) {
            iFrameEl.src = link.href;
        }
        link.addEventListener('click', getContent);
    }
    contentEl = document.getElementById('content').appendChild(iFrameEl);
}

document.addEventListener('DOMContentLoaded', init);

req.addEventListener('load', onLoad);
req.addEventListener('error', onError);
req.addEventListener('loadstart', () => {
    document.getElementById('preloader')
});