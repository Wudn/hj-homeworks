'use strict';

const req = new XMLHttpRequest();
let content;

function onError() {
    let { status, textStatus } = req;
    console.log(`Извините, возникла сетевая ошибка: status: ${status}, text: ${textStatus}`);
}

function onLoad() {
    let { status, textStatus } = req;
    if (status === 200) {
        let books = JSON.parse(req.responseText);
        books.forEach(book => {
            let liEl = document.createElement('li');
            liEl.dataset.title = book.title;
            liEl.dataset.author = book.author.name;
            liEl.dataset.info = book.info;
            liEl.dataset.price = book.price;
            liEl.innerHTML = `<img src="${book.cover.small}" >`
            content.appendChild(liEl);
        });
    } else {
        console.log(`Получен ответ от сервера: (status: ${status}, text: ${textStatus})`);
    }
}

// Регулируем видимость карточки
function toggleCardVisible () {
 document.getElementById('content').classList.toggle('hidden');
 document.getElementById('card').classList.toggle('hidden');
}

function init() {
    document.getElementById('close').addEventListener('click', toggleCardVisible);
    content = document.getElementById('content');
    content.addEventListener('click', (event) => {
        let target = null;
        if (event.target.tagName === 'LI') {
            target = event.target;
        }
        if (event.target.parentNode.tagName === 'LI') {
            target = event.target.parentNode;
        }

        if (target) {
            toggleCardVisible();
            document.getElementById('card-title').innerHTML = target.dataset.title;
            document.getElementById('card-author').innerHTML = target.dataset.author;
            document.getElementById('card-info').innerHTML = target.dataset.info;
            document.getElementById('card-price').innerHTML = target.dataset.price;
        }
    });
    
    while (content.firstChild) {
        content.removeChild(content.firstChild);
    }

    req.open('GET', 'https://neto-api.herokuapp.com/book/');
    req.send();
}

document.addEventListener('DOMContentLoaded', init);

req.addEventListener('load', onLoad);