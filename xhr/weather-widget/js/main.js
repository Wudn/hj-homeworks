'use strict';

const req = new XMLHttpRequest();

function onError() {
  console.log('Ошибка сети.');
}

function onLoad() {
  const { status, statusText } = req;
  if (status === 200) {
    const res = JSON.parse(req.responseText);
    setData(res);
  } else {
    console.log(`status: ${status}, text: ${statusText}`)
  }
}

function init() {
  req.open('GET', 'https://neto-api.herokuapp.com/weather'); // https://netology-fbb-store-api.herokuapp.com/weather
  req.send();
}

document.addEventListener('DOMContentLoaded', init);

req.addEventListener('error', onError);
req.addEventListener('load', onLoad);