'use strict';

let bindings = {
    'AltLeft': switchToDefaultSet,
    'ShiftLeft': switchToDefaultSet
}

let lowerSet = 'lower';
let middleSet = 'middle';
let higherSet = 'higher';

let currentSet = middleSet;
let aliasesKeys = ['first', 'second', 'third', 'fourth', 'fifth'];

let piano = document.getElementsByTagName('ul')[0];
let pianoKeysArr = Array.from(piano.children);

function switchSet(newSet=middleSet) {
    piano.classList.remove(currentSet);
    currentSet = newSet;
    piano.classList.add(currentSet);
}

function switchToDefaultSet() {
    if (!piano.classList.contains(middleSet)) {
        switchSet();
    }
}

function updateSet(e) {
    e.preventDefault();
    let keyCode = e.code;
    if (keyCode in bindings) {
        bindings[keyCode]();
    }
}

function playSound(e) {
    if (!(e.altKey && e.shiftKey)) {
        if (e.shiftKey) {
            if (!piano.classList.contains(lowerSet)) {
                switchSet(lowerSet);
            }
        } else if (e.altKey) {
            if (!piano.classList.contains(higherSet)) {
                switchSet(higherSet);
            }
        }
    }
    
    let player = this.children[0];

    // console.log('p', player);

    try {
        let aliasKey = aliasesKeys[pianoKeysArr.indexOf(this)];
        player.src = `sounds/${currentSet}/${aliasKey}.mp3`;
    } catch (e) {
        console.log(e);
    }
    
    player.play();
    
}

pianoKeysArr.forEach((pianoKey) => {
    pianoKey.addEventListener('click', playSound);
});

document.addEventListener('keyup', updateSet);