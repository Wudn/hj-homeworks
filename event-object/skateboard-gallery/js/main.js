'use strict';

let view = document.getElementsByClassName('gallery-view')[0];
let thumbs = document.getElementsByClassName('gallery-nav')[0].children;

function switchThumb(event) {
    event.preventDefault();
    for (let thumb of thumbs) {
        thumb.classList.remove('gallery-current');
    }
    this.classList.add('gallery-current');
    view.src = this.href;
}

for (let thumb of thumbs) {
    thumb.addEventListener('click', switchThumb);
}