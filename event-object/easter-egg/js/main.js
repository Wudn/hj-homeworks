'use strict';

let bindings = {
    KeyY() {
        checkSymbol('н');
    },
    KeyT() {
        checkSymbol('е');
    },
    KeyN() {
        checkSymbol('т');
    },
    KeyJ() {
        checkSymbol('о');
    },
    KeyK() {
        checkSymbol('л');
    },
    KeyU() {
        checkSymbol('г');
    },
    KeyB() {
        checkSymbol('и');
    },
    KeyZ() {
        checkSymbol('я');
    }
}

function checkSymbol(symbol) {
    event.preventDefault();
    secretArr.push(symbol);
    if (!(stopWord[secretArr.length-1] === secretArr[secretArr.length-1])) {
        secretArr = [];
    }
    if(stopWord.length === secretArr.length) {
        let secret = document.getElementsByClassName('secret')[0];
        secret.classList.add('visible');
    }
}

let nav = document.getElementsByTagName('nav')[0];
let stopWord = 'нетология';
let secretArr = [];

function toggleNav() {
    let className = 'visible';
    if (nav.classList.contains(className)) {
        nav.classList.remove(className);
    } else {
        nav.classList.add(className);
    }
}

function update(e) {
    if (e.ctrlKey && e.altKey && e.code === 'KeyT') {
        toggleNav();
        return;
    }
    let keyCode = event.code;
    if (keyCode in bindings) {
        bindings[keyCode]();
    }
}

document.addEventListener('keydown', update)
