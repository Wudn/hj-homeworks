'use strict';

function init() {
    let movedPiece = null,
        shiftX = 0, 
        shiftY = 0;

    document.addEventListener('mousedown', evt => {
        if(evt.target.classList.contains('logo')) {
            movedPiece = evt.target;
            movedPiece.classList.add('moving');
            const bounds = movedPiece.getBoundingClientRect();
            shiftX = evt.pageX - bounds.left - window.pageXOffset;
            shiftY = evt.pageY - bounds.top - window.pageYOffset;
        }
    });

    document.addEventListener('mousemove', evt => {
        if(movedPiece) {
            evt.preventDefault();
            movedPiece.style.left = evt.pageX - shiftX + 'px';
            movedPiece.style.top = evt.pageY - shiftY + 'px';
            
        }
    });

    document.addEventListener('mouseup', evt => {
        if (movedPiece) {
            movedPiece.style.visibility = 'hidden';
            const el = document.elementFromPoint(evt.clientX, evt.clientY);
            movedPiece.style.visibility = 'visible';
            if (el.id === 'trash_bin') {
                movedPiece.style.display = 'none';
                
            }
            movedPiece.classList.remove('moving');
            movedPiece = null;
        }
    });

    
}

document.addEventListener('DOMContentLoaded', init);