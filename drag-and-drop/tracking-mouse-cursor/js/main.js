'use strict';

function init() {
    let cursorPos = {x: 0, y: 0},
        leftEye = document.querySelector('.cat_position_for_left_eye'),
        leftPupil = document.querySelector('.cat_eye_left'),
        leftPupilBounds = leftPupil.getBoundingClientRect(),
        rightEye = document.querySelector('.cat_position_for_right_eye'),
        rightPupil = document.querySelector('.cat_eye_right'),
        rightPupilBounds = leftPupil.getBoundingClientRect();


    // console.log('leftPupilBounds', leftPupilBounds);
    // console.log('rightPupilBounds', rightPupilBounds);

    document.addEventListener('mousemove', e => {
        cursorPos.x = e.pageX;
        cursorPos.y = e.pageY;

        moveLeftPupil();
        moveRightPupil();
        // console.log('pX', e.pageX);
        // console.log('pY', e.pageY);
        
    });

    function moveLeftPupil() {
        let leftEyeBounds = leftEye.getBoundingClientRect(),
            leftEyeXCenter = leftEyeBounds.width / 2,
            leftEyeYCenter = leftEyeBounds.height / 2,
            minX = leftEyeBounds.x + leftEyeXCenter / 2,
            maxX = (leftEyeBounds.x + leftEyeBounds.width) - leftEyeXCenter / 2,
            minY = leftEyeBounds.y + leftEyeYCenter / 2,
            maxY =  (leftEyeBounds.y + leftEyeBounds.height) - leftEyeYCenter / 2;
        // if (!leftPupil.style.left) {
        //     console.log('хуета');
        //     leftPupil.style.left = `${leftPupilBounds.width}px`;
        // }
        if (cursorPos.x < leftEyeBounds.x) {
            leftPupil.style.left = `${0}px`; 
        }

        if (cursorPos.x >= minX && cursorPos.x <= maxX) {
            leftPupil.style.left = `${cursorPos.x - leftEyeBounds.x - (leftPupilBounds.width / 2)}px`; 
        }

        // if (parseInt(leftPupil.style.left) >= 0 && parseInt(leftPupil.style.left) <= 21) {
        //     console.log('t1');
        //     if (cursorPos.x >= leftEyeBounds.x && cursorPos.x <= leftEyeBounds.x + leftEyeBounds.width) {
        //         console.log('t2');
        //         // leftPupil.style.left = `${cursorPos.x - leftEyeBounds.x}px`;
        //         // if (parseInt(leftPupil.style.left) > 0 && parseInt(leftPupil.style.left) < leftPupilBounds.width) {
        //             // console.log('karamba');
        //             // if (parseInt(leftPupil.style.left)) {
        //             //     console.log('хуякс');
        //             // }
        //             leftPupil.style.left = `${cursorPos.x - leftEyeBounds.x - (leftPupilBounds.width / 2)}px`;
        //         // }
        //     }
        // }
        
        if (cursorPos.x >= leftEyeBounds.x + leftEyeBounds.width) {
            leftPupil.style.left = `${leftPupilBounds.width}px`; 
        }

        if (cursorPos.y < leftEyeBounds.y) {
            leftPupil.style.top = `${0}px`; 
        }

        if (cursorPos.y >= minY && cursorPos.y <= maxY) {
            leftPupil.style.top = `${cursorPos.y - leftEyeBounds.y - (leftPupilBounds.height / 2)}px`; 
        }
        
        if (cursorPos.y >= leftEyeBounds.y + leftEyeBounds.height) {
            leftPupil.style.top = `${leftPupilBounds.height}px`;
        }
    }

    function moveRightPupil() {
        let rightEyeBounds = rightEye.getBoundingClientRect(),
            rightEyeXCenter = rightEyeBounds.width / 2,
            rightEyeYCenter = rightEyeBounds.height / 2,
            minX = rightEyeBounds.x + rightEyeXCenter / 2,
            maxX = (rightEyeBounds.x + rightEyeBounds.width) - rightEyeXCenter / 2,
            minY = rightEyeBounds.y + rightEyeYCenter / 2,
            maxY =  (rightEyeBounds.y + rightEyeBounds.height) - rightEyeYCenter / 2;
            
        if (cursorPos.x < rightEyeBounds.x) {
            rightPupil.style.left = `${0}px`; 
        }

        if (cursorPos.x >= minX && cursorPos.x <= maxX) {
            rightPupil.style.left = `${cursorPos.x - rightEyeBounds.x - (rightPupilBounds.width / 2)}px`; 
        }

        if (cursorPos.x >= rightEyeBounds.x + rightEyeBounds.width) {
            rightPupil.style.left = `${rightPupilBounds.width}px`; 
        }

        if (cursorPos.y < rightEyeBounds.y) {
            rightPupil.style.top = `${0}px`; 
        }

        if (cursorPos.y >= minY && cursorPos.y <= maxY) {
            rightPupil.style.top = `${cursorPos.y - rightEyeBounds.y - (rightPupilBounds.height / 2)}px`; 
        }
        
        if (cursorPos.y >= rightEyeBounds.y + rightEyeBounds.height) {
            rightPupil.style.top = `${rightPupilBounds.height}px`;
        }
    }
}

document.addEventListener('DOMContentLoaded', init);



