'use strict';

function init() {
    let blockEl = document.querySelector('div.block'),
        messageEl = blockEl.querySelector('p.message'),
        textareaEl = document.querySelector('textarea.textarea'),
        isTyping = false;

        textareaEl.addEventListener('focus', evt => {
            isTyping = true;
            checkStatusOfTyping();
        });

        function debounce(callback, delay) {
            let timeout;
            return () => {
                if (!isTyping) {
                    isTyping = true;
                    callback();
                }
                clearTimeout(timeout);
                timeout = setTimeout(function() {
                    timeout = null;
                    isTyping = false;
                    callback();
                }, delay);
            }
        }

        function checkStatusOfTyping() {
            if (isTyping) {
                if (!blockEl.classList.contains('active')) {
                    blockEl.classList.add('active');
                }
                if (messageEl.classList.contains('view')) {
                    messageEl.classList.remove('view');
                }
            }
            if (!isTyping) {
                if (blockEl.classList.contains('active')) {
                    blockEl.classList.remove('active');
                }
                if (!messageEl.classList.contains('view')) {
                    messageEl.classList.add('view');
                }
            }
        }

        textareaEl.addEventListener('keydown', debounce(checkStatusOfTyping, 2000));

        textareaEl.addEventListener('blur', evt => {
            isTyping = false;
            if (messageEl.classList.contains('view')) {
                messageEl.classList.remove('view');
            }
        });


}



document.addEventListener('DOMContentLoaded', init);