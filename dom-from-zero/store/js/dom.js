'use strict';

// https://codepen.io/Disabled/pen/vvQGdp

function el(tagName, attributes, children) {
  
    const element = document .createElement(tagName);

    if (attributes !== null) {
        if (typeof attributes === 'object') { 
            Object.keys(attributes).forEach(i => element .setAttribute(i, attributes[i])); 
        } 
    }
    if (typeof children === 'string') {
        element.textContent = children;
    } else if (children instanceof Array) { 
        children.forEach(child => {
            if (typeof child === 'string') {
                element.textContent = child;
            } else {
                element.appendChild(el(child.name, child.props, child.childs));
            }
        });
    }
    return element; 
}

function createElement(node) {
    return el(node.name, node.props, node.childs);
}