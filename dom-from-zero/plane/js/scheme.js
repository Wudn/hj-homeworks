'use strict'

function init() {
    let acSelect = document.querySelector('#acSelect'),
        btnSeatMap = document.querySelector('#btnSeatMap'),
        btnSetFull = document.querySelector('#btnSetFull'),
        btnSetEmpty = document.querySelector('#btnSetEmpty'),
        titleSeatMap = document.querySelector('#seatMapTitle'),
        divSeatMap = document.querySelector('#seatMapDiv'),
        titleTotalPax = document.querySelector('#totalPax'),
        titleTotalAdult = document.querySelector('#totalAdult'),
        titleTotalHalf = document.querySelector('#totalHalf'),
        totalAdultCounter = 0,
        totalHalfCounter = 0,
        mainData,
        idPlane;

    function el(tag, attrs={}, cls=[], content=[]) {
        return {
            tag: tag,
            attrs: attrs,
            cls: cls,
            content: content
        }
    }

    function seatJSTemplate(data) {
        // console.log('md:', data);
        // console.log('Кол-во мест в ряду:', data.seats);
        let object = el('div', {}, ['row', 'seating-row', 'text-center'], []),
            seats = data.seats,
            letters,
            divider = 2,
            groupLength;
        
        object.content.push(
            el('div', {}, ['col-xs-1', 'row-number'], [
                el('h1', {}, [], data.iterator),
            ])
        );
        
        if(seats === 6) {
            letters = mainData['letters6'];
            groupLength = seats / divider;
        }
        if(seats === 4) {
            letters = mainData['letters4'];
            groupLength = seats / divider;
        }

        let counter = 0;
        if (groupLength) {
            for (let i=0; i < divider; i++) {
                let group = [];
                for (let i=0; i < groupLength; i++) {
                    group.push(
                        el('div', {}, ['col-xs-4', 'seat'], [
                            el('span', {}, ['seat-label'], letters[counter])
                        ])
                    );
                    counter++;
                }
                object.content.push(
                    el('div', {}, ['col-xs-5'], group)
                );
            }
        }

        // console.log('o', object);
        return object;
    }

    function updateTotalCounters() {
        titleTotalPax.textContent = totalAdultCounter + totalHalfCounter;
        titleTotalAdult.textContent = totalAdultCounter;
        titleTotalHalf.textContent =  totalHalfCounter;
    }

    function showScheme(event) {
        event.preventDefault();
        btnSeatMap.disabled = true;
        mainData.scheme.forEach((seats, iterator) => {
            let row = {
                seats: seats,
                iterator: iterator + 1
            }
            divSeatMap.appendChild(browserJSEngine(seatJSTemplate(row)));
        });

        if (btnSetFull.disabled) {
            btnSetFull.disabled = false;
        }
        if (btnSetEmpty.disabled) {
            btnSetEmpty.disabled = false;
        }

        updateTotalCounters();
        
    }

    function browserJSEngine(block) {
        if ((block === undefined) || (block === null) || (block === false)) {
            return document.createTextNode('');
        }
        if ((typeof block === 'string') || (typeof block === 'number') || (block === true)) {
            return document.createTextNode(block.toString());
        }
        if (Array.isArray(block)) {
            const f = document.createDocumentFragment();
            block.forEach((el) => {
                f.appendChild(
                    browserJSEngine(el)
                );
            });
            return f;
        }

        const element = document.createElement(block.tag || 'div');
        element.classList.add(...[].concat(block.cls).filter(Boolean));

        if (block.attrs) {
            Object.keys(block.attrs).forEach(key => {
                element.setAttribute(key, block.attrs[key]);
            });
        }
        if (block.content) element.appendChild(browserJSEngine(block.content));
        
        return element;
    }

    function selectPlane() {
        idPlane = acSelect.value;
        totalAdultCounter = 0;
        totalHalfCounter = 0;
        fetch(`https://neto-api.herokuapp.com/plane/${idPlane}`)
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            if (!btnSetFull.disabled) {
                btnSetFull.disabled = true;
            }
            if (!btnSetEmpty.disabled) {
                btnSetEmpty.disabled = true;
            }
            titleSeatMap.textContent = `${data.title} (${data.passengers} пассажиров)`;    
            while(divSeatMap.firstChild) {
                divSeatMap.removeChild(divSeatMap.firstChild);
            }
            
            mainData = data;
            btnSeatMap.disabled = false;
        });
    }

    function chooseSeat(event) {
        // console.log(event);
        let target = event.target;
        // console.log(target);
        let element = target.closest('div.seat');
        if (element) {
            if (!event.altKey && (element.classList.contains('half') || element.classList.contains('adult'))) {
                if (element.classList.contains('half')) {
                    element.classList.remove('half');
                    totalHalfCounter--;
                }
                if (element.classList.contains('adult')) {
                    element.classList.remove('adult');
                    totalAdultCounter--;
                }
            } else {
                if (event.altKey) {
                    if (element.classList.contains('adult')) {
                        element.classList.remove('adult');
                        totalAdultCounter--;
                    } 
                    if (!element.classList.contains('half')) {
                        element.classList.add('half');
                        totalHalfCounter++;
                    }
                } else {
                    if (!element.classList.contains('adult')) {
                        element.classList.add('adult');
                        totalAdultCounter++;
                    }
                }
            }
        }
        updateTotalCounters();
    }

    function clearSeats(event) {
        event.preventDefault();
        let seats = divSeatMap.querySelectorAll('div.seat');
        Array.from(seats).forEach(seat => {
            if (seat.classList.contains('half')) {
                seat.classList.remove('half');
            }
            if (seat.classList.contains('adult')) {
                seat.classList.remove('adult');
            }
        });
        totalHalfCounter = 0;
        totalAdultCounter = 0;
        updateTotalCounters();
    }

    function takeAllSeats(event) {
        event.preventDefault();

        let seats = divSeatMap.querySelectorAll('div.seat');
        Array.from(seats).forEach(seat => {
            if (!seat.classList.contains('adult')) {
                seat.classList.add('adult');
            }
        });

        totalAdultCounter = seats.length - totalHalfCounter;

        updateTotalCounters();
    }
    
    btnSetFull.disabled = true;
    btnSetEmpty.disabled = true;

    selectPlane();

    btnSeatMap.addEventListener('click', showScheme);

    divSeatMap.addEventListener('click', chooseSeat);

    acSelect.addEventListener('change', selectPlane);

    btnSetEmpty.addEventListener('click', clearSeats);

    btnSetFull.addEventListener('click', takeAllSeats);

}

document.addEventListener('DOMContentLoaded', init);