'use strict';

// https://codepen.io/Disabled/pen/gZQMoN

function showComments(comments) {
  const commentsContainer = document.querySelector('.comments');
  const commentNodes = comments.map(createCommentNode);
  
  const fragment = commentNodes .reduce((fragment, currentValue) => { 
    fragment.appendChild(currentValue);
    return fragment;
  }, document.createDocumentFragment());

  commentsContainer.appendChild(fragment);
}

function createCommentNode(comment) {
  // console.log(comment.text);
  return el('div', {class: 'comment-wrap'}, [
    el('div', {class: 'photo', title: comment.author.name}, [
      el('div', {class: 'avatar', style: `background-image: url(${comment.author.pic})`})
    ]),
    el('div', {class:'comment-block'}, [
      el('p', {class: 'comment-text'}, `${comment.text}`),
      el('div', {class: 'bottom-comment'}, [
        el('div', {class: 'comment-date'}, `${new Date(comment.date).toLocaleString('ru-Ru')}`),
        el('ul', {class: 'comment-actions'}, [
          el('li', {class: 'complain'}, 'Пожаловаться'),
          el('li', {class: 'reply'}, 'Ответить')
        ])
      ])
    ])
  ]);
}

function el(tagName, attributes, children) {
  const element = document .createElement(tagName);

  if (typeof attributes === 'object') { 
    Object.keys(attributes).forEach(i => element .setAttribute(i, attributes[i])); 
  } 
  if (typeof children === 'string') {
    element.setAttribute('style', 'white-space: pre;');
    element.textContent = children;
  } else if (children instanceof Array) { 
    children.forEach(child => element.appendChild(child));
  } return element; 
}

fetch('https://neto-api.herokuapp.com/comments')
  .then(res => res.json())
  .then(showComments);
