'use strict';

function init() {
    let app = document.querySelector('div.app'),
        list = document.querySelector('main.container > div.list'),
        canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d'),
        vPlayer = document.createElement('video'),
        aPlayer = document.createElement('audio'),
        errorsEl = document.querySelector('#error-message'),
        controlsEl = app.querySelector('div.controls'),
        takePhotoBtn = controlsEl.querySelector('#take-photo');

    vPlayer.id = 'video';
    aPlayer.src = './audio/click.mp3';

    app.appendChild(vPlayer);
    app.appendChild(aPlayer);

    const constraints = {
        video: true,
        audio: false
    };

    navigator.mediaDevices
        .getUserMedia(constraints)
        .then(stream => {
            controlsEl.style.display = 'block';
            controlsEl.style.textAlign = 'center';
            controlsEl.style.verticalAlign = 'bottom';
            controlsEl.style.lineHeight = controlsEl.clientHeight * 2.6 + 'px';
            takePhotoBtn.style.cursor = 'pointer';
            
            vPlayer.srcObject = stream;
            // video.src = URL.createObjectURL(stream);
            video.play();
            
            takePhotoBtn.addEventListener('click', (evt) => {
                setTimeout(() => {
                    aPlayer.play();
                    canvas.width = vPlayer.videoWidth;
                    canvas.height = vPlayer.videoHeight;
                    ctx.drawImage(vPlayer, 0, 0);
                    
                    let figureEl = document.createElement('figure'),
                        imgEl = document.createElement('img'),
                        figcaptionEl = document.createElement('figcaption'),
                        downloadEl = document.createElement('a'),
                        fileDownloadIcon = document.createElement('i'),
                        uploadEl = document.createElement('a'),
                        fileUploadIcon = document.createElement('i'),
                        deleteEl = document.createElement('a'),
                        deleteIcon = document.createElement('i');

                    imgEl.src = canvas.toDataURL();
                    figureEl.appendChild(imgEl);

                    figureEl.appendChild(figcaptionEl);
                    
                    downloadEl.download = 'snapshot.png';
                    downloadEl.href = imgEl.src;
                    figcaptionEl.appendChild(downloadEl);

                    fileDownloadIcon.classList.add('material-icons');
                    fileDownloadIcon.textContent = 'file_download';
                    downloadEl.appendChild(fileDownloadIcon);

                    figcaptionEl.appendChild(uploadEl);

                    fileUploadIcon.classList.add('material-icons');
                    fileUploadIcon.textContent = 'file_upload';
                    uploadEl.appendChild(fileUploadIcon);

                    figcaptionEl.appendChild(deleteEl);

                    deleteIcon.classList.add('material-icons');
                    deleteIcon.textContent = 'delete';
                    deleteEl.appendChild(deleteIcon);

                    list.appendChild(figureEl);

                    uploadEl.addEventListener('click', (evt) => {
                        let blob = dataURItoBlob(evt.target.closest('figure').querySelector('img').src);
                        
                        let formData = new FormData();
                        formData.append('image', blob);
                        fetch('https://neto-api.herokuapp.com/photo-booth', {
                        method: 'POST',
                        body: formData
                        })
                        .then(res => {
                            if (200 <= res.status && res.status < 300) {
                                return res;
                            }
                            throw new Error(res.statusText);
                        })
                        .then(res => res.text())
                        .then(res => console.log(res))
                        .catch((err) => {
                            console.error(err);
                        });

                        // const uploadImgRqst = new XMLHttpRequest();

                        // uploadImgRqst.addEventListener('loadend', (evnt) => {
                        //     console.log(`Ответ сервера: ${uploadImgRqst.responseText}`);
                        // });
                    
                        // uploadImgRqst.addEventListener('loadstart', (evnt) => {
                        //     console.log(`Начата отправка данных`);
                        // });
                    
                        // uploadImgRqst.open('POST', 'https://neto-api.herokuapp.com/photo-booth', true);
                    
                        // const formData = new FormData();
                        // formData.append('image', blob);
                    
                        // uploadImgRqst.send(formData);
                    });

                    deleteEl.addEventListener('click', (evt) => {
                        evt.preventDefault();
                        list.removeChild(evt.target.closest('figure'));
                    });
                    // stream.getVideoTracks().map(track => track.stop());
                }, 100);
            });
        })
        .catch(err => {
            console.error(`Ошибка доступа к медиа девайсам: ${err.message}`);
            errorsEl.textContent = `Ошибка доступа к медиа девайсам: ${err.message}`;
            errorsEl.style.display = 'block';
        });

    function dataURItoBlob(dataURI) {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataURI.split(',')[1]);
        
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        
        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);
        
        // create a view into the buffer
        var ia = new Uint8Array(ab);
        
        // set the bytes of the buffer to the correct values
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        
        // write the ArrayBuffer to a blob, and you're done
        var blob = new Blob([ab], {type: mimeString});
        return blob;
    }
}

document.addEventListener('DOMContentLoaded', init);