'use strict';

let counter;
let completeText = 'complete';
let formEl = document.querySelector('.list-block');
let outputEl = formEl.querySelector('h3 output');
let tasks = document.querySelectorAll('input[type="checkbox"]');

function updateOutput() {
    counter = 0;
    tasks.forEach(task => {
        task.addEventListener('click', updateOutput);
        if (task.checked) {
            counter++;
        }
        
        if (counter === tasks.length) {
            formEl.classList.toggle(completeText);
        } else {
            if(formEl.classList.contains(completeText)) {
                formEl.classList.toggle(completeText);
            }
        }
    });
    outputEl.innerText = `${counter} из ${tasks.length}`
}

updateOutput();





