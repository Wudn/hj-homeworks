'use strict';

let loaderEl = document.querySelector('#loader');
let contentEl = document.querySelector('#content');
let fromEl = document.querySelector('#from');
let toEl = document.querySelector('#to');
let resultEl = document.querySelector('#result');
let sourceEl = document.querySelector('#source');

let hiddenText = 'hidden';

function convert() {
    let multiplier = sourceEl.value;
    let a = fromEl.options[fromEl.selectedIndex].value;
    let b = toEl.options[toEl.selectedIndex].value;
    let result = (multiplier * a) / b;

    resultEl.innerText = result.toFixed(2);
}

loaderEl.classList.toggle(hiddenText);

let req = new XMLHttpRequest();
req.open('GET', 'https://neto-api.herokuapp.com/currency');
req.send();

function loaded() {
    let res = JSON.parse(req.responseText);
    res.forEach(item => {
        let fromOptionEl = document.createElement('option');
        fromOptionEl.value = item.value;
        fromOptionEl.innerText = item.code;
        fromEl.append(fromOptionEl);
        
        let toOptionEl = document.createElement('option');
        toOptionEl.value = item.value;
        toOptionEl.innerText = item.code;
        toEl.append(toOptionEl);
    });
    convert();
    loaderEl.classList.toggle(hiddenText);
    contentEl.classList.toggle(hiddenText);
}

sourceEl.addEventListener('input', convert);
fromEl.addEventListener('change', convert);
toEl.addEventListener('change', convert);
req.addEventListener('load', loaded);





