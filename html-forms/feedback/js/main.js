'use strict';

let contentForm = document.querySelector('.contentform');
let items = contentForm.querySelectorAll('input, textarea');
let zipInput = contentForm.querySelector('input[name="zip"]');
let output = document.querySelector('#output');
let submitBtn = document.querySelector('button[type="submit"]');
let changeMessageBtn = document.querySelector('main > .button-contact');
let mainOutputs = document.querySelectorAll('main output')

items.forEach(item=>item.addEventListener('input', checkFieldsLen));

function checkFieldsLen() {
    let data = Array.from(items).filter(item => item.value.length <= 0);
    if (data.length === 0) {
        if(submitBtn.hasAttribute('disabled')) {
            submitBtn.toggleAttribute('disabled');
        }
    } else {
        if(!submitBtn.hasAttribute('disabled')) {
            submitBtn.toggleAttribute('disabled');
        }
    }
}

// Переключение между формами, заполнение сообщения данными
submitBtn.addEventListener('click', () => {
    event.preventDefault();
    contentForm.classList.toggle('hidden');
    output.classList.toggle('hidden');
    items.forEach(item => {
        mainOutputs.forEach(output => {
            if (output.id === item.name) {
                output.value = item.value;
            }
        });
    });
});

changeMessageBtn.addEventListener('click', () => {
    contentForm.classList.toggle('hidden');
    output.classList.toggle('hidden');
});

checkFieldsLen();
zipInput.type = "number";
zipInput.min = "0";
zipInput.max = "999999";