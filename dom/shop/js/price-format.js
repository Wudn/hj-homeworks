'use strict';

let cartCountEl;
let cartTotalPrice;
const products = [];
const reducer = (accumulator, currentValue) => +accumulator + +currentValue;

function getPriceFormatted(value) {
  return  value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}

function addProduct() {
  products.push(this.dataset.price);
  cartCountEl.innerText = products.length;
  cartTotalPrice.innerText = getPriceFormatted(products.reduce(reducer));
}

function init() {
  cartCountEl = window['cart-count'];
  cartTotalPrice = window['cart-total-price'];

  let addButtons = document.querySelectorAll('.add');
  for (let addButton of addButtons) {
    addButton.addEventListener('click', addProduct);
  }
}

document.addEventListener('DOMContentLoaded', init);