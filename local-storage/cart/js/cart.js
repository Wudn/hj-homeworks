'use strict';
// Хранение состояния на клиенте и отправка на сервер
// 1 задание https://codepen.io/Disabled/pen/yGXPPK?editors=0010
// 2 задание https://codepen.io/Disabled/pen/qLjVQW?editors=0010
// 3 задание 

function init() {
    let colorSwatchEl = document.getElementById('colorSwatch'),
        sizeSwatchEl = document.getElementById('sizeSwatch'),
        quickCartEl = document.getElementById('quick-cart'),
        addToCartBtn = document.getElementById('AddToCart'),
        addToCartForm = document.getElementById('AddToCartForm');

    let quickCartProduct,
        quickCartProductWrap,
        imgEl,
        s1Span,
        s2Span,
        countSpan,
        quickCartProductRemoveSpan,
        quickCartPayEl,
        quickCartPaySpan,
        quickCartPayStrong,
        quickCartPayBr,
        quickCartPrice;

    function renderItems() {
        quickCartProduct = document.createElement('div');
        quickCartProduct.classList.add('quick-cart-product', 'quick-cart-product-static');

        quickCartProductWrap = document.createElement('div');
        quickCartProductWrap.classList.add('quick-cart-product-wrap');
        quickCartProduct.appendChild(quickCartProductWrap);

        imgEl = document.createElement('img');
        quickCartProductWrap.appendChild(imgEl);

        s1Span = document.createElement('span');
        s1Span.classList.add('s1');
        s1Span.style.backgroundColor = '#000';
        s1Span.style.opacity = '0.5';

        s2Span = document.createElement('span');
        s2Span.classList.add('s2');
        quickCartProductWrap.appendChild(s2Span);

        countSpan = document.createElement('span');
        countSpan.classList.add('count', 'hide', 'fadeUp');
        quickCartProduct.appendChild(countSpan);

        quickCartProductRemoveSpan = document.createElement('span');
        quickCartProductRemoveSpan.classList.add('quick-cart-product-remove', 'remove');
        quickCartProduct.appendChild(quickCartProductRemoveSpan);

        quickCartProductRemoveSpan.addEventListener('click', removeProduct);

        quickCartPayEl = document.createElement('a');
        quickCartPayEl.id = 'quick-cart-pay';
        quickCartPayEl.setAttribute('quickbeam', 'cart-pay');
        quickCartPayEl.classList.add('cart-ico', 'open');

        quickCartPaySpan = document.createElement('span');
        quickCartPayEl.appendChild(quickCartPaySpan);

        quickCartPayStrong = document.createElement('strong');
        quickCartPayStrong.classList.add('quick-cart-text');
        quickCartPayStrong.textContent = 'Оформить заказ';

        quickCartPayBr = document.createElement('br');
        quickCartPayStrong.appendChild(quickCartPayBr);
        quickCartPaySpan.appendChild(quickCartPayStrong);

        quickCartPrice = document.createElement('span');
        quickCartPrice.id = 'quick-cart-price';
        quickCartPaySpan.appendChild(quickCartPrice);
        quickCartEl.appendChild(quickCartPayEl);

        quickCartEl.appendChild(quickCartProduct);
    }

    function removeProduct(event) {
        // console.log(event.currentTarget);
        // let object = {
        //     'productId': event.currentTarget.dataset.id
        // };
        let formData = new FormData();
        formData.append('productId', event.currentTarget.dataset.id);
        // let data = JSON.stringify(object);
        // console.log(data);
        fetch('https://neto-api.herokuapp.com/cart/remove', {
            body: formData,
            // headers: {
            //     'Content-Type': 'application/json'
            // },
            method: 'POST',
            credentials: 'same-origin'
        })
        .then((res) => {
            if (200 <= res.status && res.status < 300) {
                return res;
            }
            throw new Error(res.statusText);
        })
        .then(res => res.json())
        .then((res) => {
            if (res.error) {
                console.error(res.message);
            } else {
                if (res.length <= 0) {
                    while (quickCartEl.firstChild) {
                        quickCartEl.removeChild(quickCartEl.firstChild);
                        quickCartProductWrap = undefined;
                    }
                } else {
                    res.forEach(product => {
                        updateData(product);
                    });
                }
            }
        })
        .catch(error => {
            console.error(error);
        });
    }

    function updateData(product) {
        quickCartProduct.id = `quick-cart-product-${product.id}`;
        imgEl.src = product.pic;
        imgEl.setAttribute('title', product.title);
        
        s1Span.textContent = `$${product.price}`;
        quickCartProductWrap.appendChild(s1Span);

        countSpan.textContent = product.quantity;
        countSpan.id = `quick-cart-product-count-${product.id}`;

        quickCartProductRemoveSpan.dataset.id = product.id;
        quickCartPrice.textContent = `$${(Number(product.price) * product.quantity).toFixed(2)}`;
    }

    function addToCart(event) {
        event.preventDefault();
        // let object = {};
        let formData = new FormData(addToCartForm);
        formData.append('productId', addToCartForm.dataset.productId);

        // for (const[k, v] of formData) {
        //     object[k] = v;
        // }
        // console.log(JSON.stringify(object));
        fetch('https://neto-api.herokuapp.com/cart', {
            method: 'POST',
            // headers: {
            //     'Content-Type': 'application/json'
            // },
            body: formData
        })
        .then(res => {
            if (200 <= res.status && res.status < 300) {
                return res;
            }
            throw new Error(res.statusText);
        })
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                console.error(res.message);
            } else {
                res.forEach((product) => {
                    console.log(product);
                    console.log(quickCartProductWrap);
                    if(!quickCartProductWrap) {
                        renderItems();
                    }
                    updateData(product);
                });
            }
        })
        .catch(error => {
            console.error(error);
        });
    }

    addToCartBtn.addEventListener('click', addToCart)
    Promise.all([   
        fetch('https://neto-api.herokuapp.com/cart/colors'), 
        fetch('https://neto-api.herokuapp.com/cart/sizes'),
        fetch('https://neto-api.herokuapp.com/cart')
    ])
    .then(([res1, res2, res3]) => {
        if ((200 <= res1.status && res1.status < 300) && (200 <= res2.status && res2.status < 300) && (200 <= res3.status && res3.status < 300)) {
            return [res1, res2, res3];
        }
        throw new Error(`res1: ${res.statusText}, res2: ${res2.statusText}, res3: ${res3.statusText}`);
    })
    .then(([res1, res2, res3]) => {
        return [res1.json(), res2.json(), res3.json()];
    })
    .then(([colors, sizes, products]) => {
        colors.then(colors => {
            console.log(colors);
            colors.forEach((color, index) => {
                // console.log(`color: ${color}, index: ${index}`)
                let swatchEl = document.createElement('div');
                swatchEl.dataset.value = color.type;
                swatchEl.classList.add('swatch-element', 'color', color.type, color.isAvailable ? 'available' : 'soldout');

                let tooltipEl = document.createElement('div');
                tooltipEl.classList.add('tooltip');
                tooltipEl.textContent = color.title;
                swatchEl.appendChild(tooltipEl);

                let inputEl = document.createElement('input');
                inputEl.setAttribute('quickbeam', 'color');
                inputEl.id = `swatch-1-${color.type}`; // `swatch-${index + 1}-${color.type}`
                inputEl.type = 'radio';
                inputEl.name = 'color';
                inputEl.value = color.type;
                if (color.type === "red") {
                    inputEl.checked = true;
                }
                if (!color.isAvailable) {
                    inputEl.setAttribute('disabled', 'disabled');
                }
                swatchEl.appendChild(inputEl);

                let labelEl = document.createElement('label');
                labelEl.setAttribute('for', `swatch-1-${color.type}`);
                // labelEl.style.borderColor = color.code;
                swatchEl.appendChild(labelEl);

                let spanEl = document.createElement('span');
                spanEl.style.backgroundColor = color.code;
                labelEl.appendChild(spanEl);

                let imgEl = document.createElement('img');
                imgEl.classList.add('crossed-out');
                imgEl.src = 'https://neto-api.herokuapp.com/hj/3.3/cart/soldout.png?10994296540668815886';
                labelEl.appendChild(imgEl);

                colorSwatchEl.appendChild(swatchEl);
            });
        });
        sizes.then(sizes => {
            console.log(sizes);
            sizes.forEach((size, index) => {
                // console.log(`color: ${color}, index: ${index}`)
                let swatchEl = document.createElement('div');
                swatchEl.dataset.value = size.type;
                swatchEl.classList.add('swatch-element', 'plain', size.type, size.isAvailable ? 'available' : 'soldout');

                let inputEl = document.createElement('input');
                inputEl.id = `swatch-0-${size.type}`; // `swatch-${index + 1}-${color.type}`
                inputEl.type = 'radio';
                inputEl.name = 'size';
                inputEl.value = size.type;
                if (size.type === "xl") {
                    inputEl.checked = true;
                }
                if (!size.isAvailable) {
                    inputEl.setAttribute('disabled', 'disabled');
                }
                swatchEl.appendChild(inputEl);

                let labelEl = document.createElement('label');
                labelEl.setAttribute('for', `swatch-0-${size.type}`);
                labelEl.textContent = size.title;
                swatchEl.appendChild(labelEl);

                let imgEl = document.createElement('img');
                imgEl.classList.add('crossed-out');
                imgEl.src = 'https://neto-api.herokuapp.com/hj/3.3/cart/soldout.png?10994296540668815886';
                labelEl.appendChild(imgEl);

                sizeSwatchEl.appendChild(swatchEl);
            })
        });
        products.then(products => {
            if (products.length > 0) {
                products.forEach(product => {
                    renderItems();
                    updateData(product);
                });
            }
        })
    })
    .catch(([error1, error2]) => {
        console.error(error1);
        console.error(error2);
    });
}

document.addEventListener('DOMContentLoaded', init);