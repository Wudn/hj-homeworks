'use strict';

function init() {
    let loginFormEl = document.querySelector('.login-form');

    function action(event) {
        event.preventDefault();
        let target = event.target,
            errors = [],
            form,
            output,
            url,
            object = {},
            data;

        if(target.value === 'Войти') {
            form = target.closest('form');
            url = 'https://neto-api.herokuapp.com/signin';
            data = new FormData(form);
        }

        if(target.value === 'Зарегистрироваться') {
            form = target.closest('form');
            url = 'https://neto-api.herokuapp.com/signup';
            data = new FormData(form);
        }

        if (!url) {
            errors.push('Не задан url');
        }
        if (!data) {
            errors.push('Не задан FormData');
        }

        if (errors.length > 0) {
            return;
        }

        for (const[k, v] of data) {
            object[k] = v;
        }
      
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(object)
          }).then((res) => {
            if (200 <= res.status && res.status < 300) {
                return res;
            }
            throw new Error(res.statusText);
          })
          .then((res) => res.json())
          .then((data) => {
            // console.log(data);
            if(data.error) {
                output = form.querySelector('output');
                let errorMessage = data.message;
                output.textContent = errorMessage;
            } else {
                output = form.querySelector('output');
                // console.log(form);
                if (form.classList.contains('sign-in-htm')) {
                    output.textContent = `Пользователь ${data.name} успешно авторизован`;
                }
                if(form.classList.contains('sign-up-htm')) {
                    output.textContent = `Пользователь ${data.name} успешно зарегистрирован`;
                }
                
            }
          })
          .catch((error) => {
            console.error(error);
          });
    }

    loginFormEl.addEventListener('click', action);
}

document.addEventListener('DOMContentLoaded', init);