'use strict';

function init() {
    let wrapBtnsEl = document.querySelector('.wrap-btns'),
        counterEl = document.querySelector('#counter'),
        counter = localStorage.counter;
        if (!counter) {
            counter = localStorage.counter = String(0);
        }
        counterEl.textContent = counter;

    function action(event) {
        let target = event.target;
        switch (target.id) {
            case 'increment':
                counter++;
                break;
            case 'decrement':
                counter > 0 ? counter-- : console.log('Остановитесь!');
                break;
            case 'reset':
                counter = 0;
                break;
            default:
                break;
        }
        localStorage.counter = String(counter);
        counterEl.textContent = counter;
    }

    wrapBtnsEl.addEventListener('click', action);
}

document.addEventListener('DOMContentLoaded', init);