const prop = ( data, name ) => data.map( item => item[ name ] ),
  summ = data => data.reduce(( total, value ) => total + value, 0 );
class SpriteGenerator {
  constructor( container ) {
    this.uploadButton = container.querySelector( '.sprite-generator__upload' );
    this.submitButton = container.querySelector( '.sprite-generator__generate' );
    this.imagesCountContainer = container.querySelector( '.images__added-count-value' );
    this.codeContainer = container.querySelector( '.sprite-generator__code' );
    this.imageElement = container.querySelector( '.sprite-generator__result-image' );
    this.images = [];

    this.imagesCount = 0;

    this.registerEvents();
  }
  registerEvents() {
    this.uploadButton.addEventListener('change', evt => {
      Array.from(this.uploadButton.files).forEach(file => {
        let img = new Image();
        img.src = URL.createObjectURL(file);
        this.images.push(img);
      });
      this.imagesCountContainer.textContent = this.images.length;
    });

    this.submitButton.addEventListener('click', evt => {
      let canvas = document.createElement("canvas"),
          ctx = canvas.getContext("2d"),
          codeArr = [],
          collect = data => data.reduce(( total, value ) => total + value);
      canvas.width = 128 * this.images.length;
      canvas.height = 128;
      this.images.forEach((img, index) => {
        ctx.drawImage(img, 128 * index, 0, 128, 128);
        codeArr.push(`.icon {
          display: inline-block;
          background-image: url("${img.src}");
        }
        
        .icon_like {
          background-position: ${128 * index}px 0;
          width: 128px;
          height: 128px;
        }
        
        `.replace(/ {2,}/g, ' '));
      });
      console.log(codeArr);
      this.codeContainer.textContent = collect(codeArr);
      this.imageElement.src = canvas.toDataURL();
    });
  }
}

new SpriteGenerator( document.getElementById( 'generator' ));
