'use strict';

const addClass = ( className, context ) => context.classList.add( className ),
  removeClass = ( className, context ) => context.classList.remove( className ),
  hasClass = ( className, context ) => context.classList.contains( className );
class iLayout {
  constructor( container ) {
    this.container = container;
    this.positionsContainer = container.querySelector( '.layout__positions' );
    this.actionButton = container.querySelector( '.layout__button' );
    this.result = container.querySelector( '.layout__result' );
    this.layout = {
      left: null,
      top: null,
      bottom: null
    };
    this.registerEvents();
  }
  registerEvents() {
    // console.log(this.registerEvents.name);
    this.positionsContainer.addEventListener('dragover', evt => {
      evt.preventDefault();
      if (!hasClass('layout__item_active', evt.target)) {
        addClass('layout__item_active', evt.target);
      }
    });
    this.positionsContainer.addEventListener('dragleave', evt => {
      if (hasClass('layout__item_active', evt.target)) {
        removeClass('layout__item_active', evt.target);
      }
    });
    this.positionsContainer.addEventListener('drop', evt => {
      evt.preventDefault();
      if (hasClass('layout__item_active', evt.target)) {
        removeClass('layout__item_active', evt.target);
      }
      const files = Array.from(evt.dataTransfer.files);
      
      files.forEach(file => {
        if (/image.*/.test(file.type)) {
          let selectedEl = evt.target.closest('.layout__item'),
              canvas = document.createElement('canvas'),
              ctx = canvas.getContext('2d');

          while(selectedEl.firstChild) {
            selectedEl.removeChild(selectedEl.firstChild);
          }

          canvas.width = selectedEl.clientWidth;
          canvas.height = selectedEl.clientHeight;

          let imgEl = new Image();
          imgEl.addEventListener('load', () => {
            ctx.drawImage(imgEl, 0, 0);
            imgEl = new Image();
            imgEl.classList.add('layout__image');
            imgEl.addEventListener('load', () => {
              selectedEl.appendChild(imgEl);
            })
            imgEl.src = canvas.toDataURL();
            
          });

          imgEl.src = URL.createObjectURL(file);          
        } else {
          console.error('Файл должен быть картинкой');
        }
      });

    });
    this.actionButton.addEventListener('click', evt => {
      let images = Array.from(this.positionsContainer.querySelectorAll('.layout__image')),
          canvas = document.createElement('canvas'),
          ctx = canvas.getContext('2d');

          canvas.width = this.positionsContainer.clientWidth;
          canvas.height = this.positionsContainer.clientHeight;

          let curWidth = 0,
              curHeight = 0,
              maxHeight = this.positionsContainer.clientHeight;

          console.log('cW', curWidth);
          console.log('cH', curHeight);

          images.forEach(img => {
            let bounds = img.getBoundingClientRect();
            console.log('i', img);
            console.log('b', bounds);
            // console.log('Отрисовал картинку');
            
            ctx.drawImage(img, curWidth, curHeight);
            curHeight += bounds.height;

            console.log(`compare (curHeight:${curHeight} === maxHeight:${maxHeight}):`, curHeight === maxHeight);
            if (curHeight === maxHeight || curHeight-1 === maxHeight) {
              curWidth += bounds.width;
              curHeight = 0;
            }

            console.log('cW', curWidth);
            console.log('cH', curHeight);
          });
          let imgEl = new Image();

          imgEl.addEventListener('load', () => {
            this.result.value = '';
            this.result.value = imgEl.outerHTML;
          });

          imgEl.src = canvas.toDataURL();
    });
  }
}

new iLayout( document.getElementById( 'layout' ));



// loadFile( e ) {
//   // console.log(e);
//   e.preventDefault();
//   this.hideHint();
//   const files = Array.from(e.dataTransfer.files);
//   // console.log(files);
//   const readFile = this.readFile.bind( this );
//   files.forEach(readFile);
// }
// readFile( file ) {
// const reader = new FileReader();
// this.setFilename(file.name);
// reader.addEventListener('load', event => {
//   this.contentContainer.value = event.target.result;
//   this.save();
// });
