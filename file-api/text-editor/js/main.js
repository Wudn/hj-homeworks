const throttle = ( handler, ms ) => {
  let timeout;
  return () => {
    clearTimeout( timeout );
    timeout = setTimeout( handler, ms );
  }
};
class TextEditor {
  constructor( container, storageKey = '_text-editor__content' ) {
    this.container = container;
    this.contentContainer = container.querySelector( '.text-editor__content' );
    this.hintContainer = container.querySelector( '.text-editor__hint' );
    this.filenameContainer = container.querySelector( '.text-editor__filename' );
    this.storageKey = storageKey;
    this.registerEvents();
    this.load( this.getStorageData());
  }
  registerEvents() {
    const save = throttle( this.save.bind( this ), 1000 );
    // const loadFile = this.loadFile.bind( this );
    this.contentContainer.addEventListener( 'input', save );
    this.container.addEventListener('dragover', event => {
      this.showHint.call(this, event);
    });
    this.container.addEventListener('drop', event => {
      this.loadFile.call(this, event);
    });
    // this.contentContainer.addEventListener('drop', loadFile);
  }
  loadFile( e ) {
      // console.log(e);
      e.preventDefault();
      this.hideHint();
      const files = Array.from(e.dataTransfer.files);
      // console.log(files);
      const readFile = this.readFile.bind( this );
      files.forEach(readFile);
  }
  readFile( file ) {
    const reader = new FileReader();
    this.setFilename(file.name);
    if (file.type === 'text/plain') {
      reader.addEventListener('load', event => {
        this.contentContainer.value = event.target.result;
        this.save();
      });
  
      this.contentContainer.innerHTML = '';
      reader.readAsText(file);
    } else {
      console.error('Файлы должны иметь расширение: .txt');
    }
  }
  setFilename( filename ) {
    this.filenameContainer.textContent = filename;
  }
  showHint( e ) {
    e.preventDefault();
    this.hintContainer.classList.add('text-editor__hint_visible');
  }
  hideHint() {
    this.hintContainer.classList.remove('text-editor__hint_visible');
  }
  load( value ) {
    this.contentContainer.value = value || '';
  }
  getStorageData() {
    return localStorage[ this.storageKey ];
  }
  save() {
    localStorage[ this.storageKey ] = this.contentContainer.value;
  }
}

new TextEditor( document.getElementById( 'editor' ));
