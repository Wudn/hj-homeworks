'use strict';

let trackList = [
    {
        name: 'LA Chill Tour',
        link: 'https://netology-code.github.io/hj-homeworks/html-element-collection/audioplayer/mp3/LA Chill Tour.mp3'
    },
    {
        name: 'This is it band',
        link: 'https://netology-code.github.io/hj-homeworks/html-element-collection/audioplayer/mp3/This is it band.mp3'
    },
    {
        name: 'LA Fusion Jam',
        link: 'https://netology-code.github.io/hj-homeworks/html-element-collection/audioplayer/mp3/LA Fusion Jam.mp3'
    }
]

let trackNumber = 0;

function setTrack() {
    trackTitle.title = trackList[trackNumber].name;
    player.src = trackList[trackNumber].link;
}

function toggleMediaPlayer() {
    let className = 'play';
    if (!mediaPlayer.classList.contains(className)) {
        mediaPlayer.classList.add(className);
    } else {
        mediaPlayer.classList.remove(className);
    }
}

function stopSound() {
    if (!player.paused) {
        player.pause();
        toggleMediaPlayer();
    }
    
    player.currentTime = 0;
    
}

function toggleSound() {
    if (player.paused) {
        player.play();
        toggleMediaPlayer();
    } else {
        player.pause();
        toggleMediaPlayer();
    }
}

function previousTrack() {
    trackNumber -= 1;
    if (trackNumber < 0) {
        trackNumber = trackList.length-1;
    }

    if (!player.paused) {
        setTrack();
        player.play();
    } else {
        setTrack();
    }
}

function nextTrack() {
    trackNumber += 1;
    if (trackNumber === trackList.length) {
        trackNumber = 0;
    }
    
    if (!player.paused) {
        setTrack();
        player.play();
    } else {
        setTrack();
    }
}

let mediaPlayer = document.getElementsByClassName('mediaplayer')[0];
let trackTitle = document.getElementsByClassName('title')[0];

let player = document.getElementsByTagName('audio')[0];

let playstateBtn = document.getElementsByClassName('playstate')[0];

let stopBtn = document.getElementsByClassName('stop')[0];

let prevBtn = document.getElementsByClassName('back')[0];
let nextBtn = document.getElementsByClassName('next')[0];

player.ontimeupdate = function() {
    if ((player.currentTime === player.duration) && player.loop == false) {
        toggleMediaPlayer();
    }
}

playstateBtn.onclick = toggleSound;
stopBtn.onclick = stopSound;
prevBtn.onclick = previousTrack;
nextBtn.onclick = nextTrack;

setTrack();
