'use strict';

function playSound() {
    let player = this.getElementsByTagName('audio')[0];
    if (event.type === 'click') {
        player.play();
    }
    if (event.type === 'mouseup') {
        player.pause();
        player.currentTime = 0;
    }
}

let drumKit = document.getElementsByClassName('drum-kit')[0];
let buttons = drumKit.children;

for (let button of buttons) {
    button.onclick = playSound;
    button.onmouseup = playSound;
}