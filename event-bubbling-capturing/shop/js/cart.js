'use strict';

function init() {
    function action(event) {
        event.preventDefault();
        let target = event.target;
        addToCart({title: target.dataset.title, price: target.dataset.price});
    }
    
    list.addEventListener('click', action);
}

document.addEventListener('DOMContentLoaded', init);