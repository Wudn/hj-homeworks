'use strict';

function handleTableClick(event) {
    let target = event.target;
    let currentTarget = event.currentTarget;
   
    if(target.tagName === 'TH') {
        switch (target.dataset.dir) {
            case '-1':
                target.dataset.dir = 1;
                break;
            case '1':
                target.dataset.dir = -1;
                break;
            default:
                target.dataset.dir = 1;
                break;
        }
        
        currentTarget.dataset.sortBy = target.dataset.propName;
        sortTable(target.dataset.propName, target.dataset.dir);
    }
}
