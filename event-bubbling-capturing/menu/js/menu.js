'use strict';

function init() {
  const containerEl = document.querySelector('div.container > ul'),
        dropdownsList = containerEl.querySelectorAll('.dropdown'),
        dropdownsMenuList = containerEl.querySelectorAll('ul.dropdown-menu > li');

  function toggleMenu(event) {
    let currentTarget = event.currentTarget;
    if (currentTarget.classList.contains('show')) {
      currentTarget.classList.remove('show');
      currentTarget.classList.add('hide');
    } else {
      currentTarget.classList.add('show');
      currentTarget.classList.remove('hide');
    }
  }
 
  function showProducts(event) {
    let currentTarget = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();
    console.log(currentTarget.textContent);
  }
  Array
  .from(dropdownsList)
  .forEach(el => {
    el.addEventListener('click', toggleMenu);
  });

  Array
  .from(dropdownsMenuList)
  .forEach(el => {
    el.addEventListener('click', showProducts);
  });
}

document.addEventListener('DOMContentLoaded', init);
