'use strict';

function initWS() {

    let conn = new WebSocket('wss://neto-api.herokuapp.com/comet/websocket');

    conn.addEventListener('message', (data) => {
            try {
                Helper.showRandomNumber(data.data, 'websocket');
            } catch (err) {
                console.error(err);
            }
    });
}

document.addEventListener('DOMContentLoaded', initWS)