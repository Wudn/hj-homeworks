'use strict';

function initLongPolling() {
    function longPolling () {
        fetch('https://neto-api.herokuapp.com/comet/long-pooling')
        .then(data => {
            // console.log(data.headers.get('Content-Type'));// application/json; charset=utf-8
            // console.log(data.status); // 200
            return data.json();
        })
        .then(data => {
            Helper.showRandomNumber(data, 'long-polling');
        })
        .catch(err => {
            console.error(err);
        });
    }
    
    setInterval(longPolling, 20000);
}

document.addEventListener('DOMContentLoaded', initLongPolling);