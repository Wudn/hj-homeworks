'use strict';

var Helper = {
    showRandomNumber: function(data, comet) {
        let containers = document.querySelectorAll(`.${comet} div`),
            numbers = [];
        data = +data;
        // console.log(data);
        // console.log(comet, containers);
        
        Array.from(containers).forEach(container => {
            numbers.push(+(container.textContent));
            if(container.classList.contains('flip-it'))  {
                container.classList.remove('flip-it');
            }
        });
        // console.log(numbers);
        if (numbers.includes(data)) {
            Array.from(containers)[data-1].classList.add('flip-it');
        } else {
            throw new Error('Неизвестная ошибка');
        }

    }
}

function initPolling() {
    
    function polling () {
        fetch('https://neto-api.herokuapp.com/comet/pooling')
        .then(data => {
            // console.log(data.headers.get('Content-Type'));// application/json; charset=utf-8
            // console.log(data.status); // 200
            return data.json();
        })
        .then(data => {
            Helper.showRandomNumber(data, 'polling');
        })
        .catch(err => {
            console.error(err);
        });
    }
    
    setInterval(polling, 5000);
}

document.addEventListener('DOMContentLoaded', initPolling);