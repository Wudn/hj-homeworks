'use strict';

function init() {
    const conn = new WebSocket('wss://neto-api.herokuapp.com/draw');

    function updateCanvas(evt) {
        evt.canvas.toBlob(blob => conn.send(blob));
    }

    conn.addEventListener('open', (evt) => {
        // console.log('open', evt);
        editor.addEventListener('update', updateCanvas);
    });
    
    conn.addEventListener('message', evt => {
        console.log(evt);
    });

    conn.addEventListener('close', (evt) => {
        editor.removeEventListener('update', updateCanvas);
    });
    
    conn.addEventListener('error', (err) => {
        console.error(err.data);
    })
    
    window.addEventListener('beforeunload', () => {
        conn.close(1000, 'Соединение закрыто');
    });

}

document.addEventListener('DOMContentLoaded', init);